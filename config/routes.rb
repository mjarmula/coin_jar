Rails.application.routes.draw do
  root to: 'home#index'

  namespace :api do
    namespace :v1 do
      resources :currencies, only: %w[create index]
    end
  end
end
