require 'rails_helper'

describe Api::V1::CurrenciesController, type: :controller do
  context 'when there is no currency' do
    before do
      get :index
    end

    let(:expected_response) { [] }

    it 'returns empty array' do
      expect(response).to have_http_status(:ok)
      expect(JSON.parse(response.body)).to eq(expected_response)
    end
  end

  context 'when there are few currencies' do
    let(:ask) { "13720.0" }
    let(:bid) { "13720.0" }
    let(:last) { "13720.0" }

    before do
      create(
        :currency,
        ask: ask,
        bid: bid,
        last: last,
        name: 'btcaud',
        current_time: Time.zone.now - 2.days
      )
      create(
        :currency,
        ask: ask,
        bid: bid,
        last: last,
        name: 'ethaud',
        current_time: Time.zone.now
      )

      get :index
    end
    let(:expected_response) do
      [
        {
          'name' => 'ethaud',
          'ask' => ask,
          'bid' => bid,
          'last' => last
        },
        {
          'name' => 'btcaud',
          'ask' => ask,
          'bid' => bid,
          'last' => last
        }
      ]
    end

    it 'returns array of currencies ordered by current_time descending' do
      expect(response).to have_http_status(:ok)
      expect(JSON.parse(response.body)).to eq(expected_response)
    end
  end
end
