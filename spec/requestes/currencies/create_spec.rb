require 'rails_helper'

describe Api::V1::CurrenciesController, type: :controller do
  before do
    Currency::DEFINED_NAMES = %w[BTCAUD ETHAUD]
  end

  describe 'POST #create' do
    context 'when coin jar response is successfull' do
      let(:coin_jar_response) do
        "{\"volume_24h\":\"46.50400000\",\"volume\":\"16.82600000\",\"transition_time\":\"2020-03-05T23:50:00Z\",\"status\":\"continuous\",\"session\":21900,\"prev_close\":\"13790.00000000\",\"last\":\"#{last}\",\"current_time\":\"2020-03-05T23:07:18.998591Z\",\"bid\":\"#{bid}\",\"ask\":\"#{ask}\"}"
      end
      let(:status) { 200 }
      let(:bid) { "13720.0" }
      let(:ask) { "13760.0" }
      let(:last) { "13740.0" }
      let(:expected_response) do
        [
          {"bid"=>bid, "ask"=>ask, "last"=>last, "name"=>"BTCAUD"},
          {"bid"=>bid, "ask"=>ask, "last"=>last, "name"=>"ETHAUD"}
        ]
      end

      before do
        stub_coin_jar_request('BTCAUD', status, coin_jar_response)
        stub_coin_jar_request('ETHAUD', status, coin_jar_response)

        post :create
      end

      it 'returns expected response' do
        expect(response).to have_http_status(:created)
        expect(JSON.parse(response.body)).to eq(expected_response)
      end
    end

    context 'when coin jar response is not successful' do
      let(:coin_jar_response) { "{\"error_type\":\"NOT_FOUND\",\"error_messages\":[\"Record not found.\"]}" }
      let(:status) { 500 }
      let(:expected_response) do
        {
          "error" => I18n.t('currency.import.error')
        }
      end

      before do
        stub_coin_jar_request('BTCAUD', status, coin_jar_response)
        stub_coin_jar_request('ETHAUD', status, coin_jar_response)

        post :create
      end

      it 'returns error message' do
        expect(response).to have_http_status(:unprocessable_entity)
        expect(JSON.parse(response.body)).to eq(expected_response)
      end
    end
  end

  private

  def stub_coin_jar_request(name, status, response)
    url = "https://data.exchange.coinjar.com/products/#{name}/ticker"

    stub_request(:get, url).to_return(status: status, body: response)
  end
end
