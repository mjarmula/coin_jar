require 'rails_helper'

shared_context 'coin jar get request' do
  let(:name) { 'BTCAUD' }
  let(:expected_url) do
    "https://data.exchange.coinjar.com/products/#{name}/ticker"
  end
  let(:stubbed_response) do
    "{\"volume_24h\":\"46.50400000\",\"volume\":\"16.82600000\",\"transition_time\":\"2020-03-05T23:50:00Z\",\"status\":\"continuous\",\"session\":21900,\"prev_close\":\"13790.00000000\",\"last\":\"13740.00000000\",\"current_time\":\"2020-03-05T23:07:18.998591Z\",\"bid\":\"13720.00000000\",\"ask\":\"13760.00000000\"}"
  end

  let(:success_status) { 200 }

  before do
    stub_request(:get, expected_url).to_return(status: success_status, body: stubbed_response)
  end
end
