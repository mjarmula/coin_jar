require 'rails_helper'

describe Currencies::Importers::CoinJar do
  describe '.import_currency' do
    let(:faraday_response) do
      instance_double('Faraday::Response', body: response_body, success?: status)
    end
    let(:response_body) do
      "{\"volume_24h\":\"46.50400000\",\"volume\":\"16.82600000\",\"transition_time\":\"2020-03-05T23:50:00Z\",\"status\":\"continuous\",\"session\":21900,\"prev_close\":\"13790.00000000\",\"last\":\"#{last}\",\"current_time\":\"2020-03-05T23:07:18.998591Z\",\"bid\":\"#{bid}\",\"ask\":\"#{ask}\"}"
    end
    let(:payload) do
      {
        "volume_24h"=>"46.50400000",
        "volume"=>"16.82600000",
        "transition_time"=>"2020-03-05T23:50:00Z",
        "status"=>"continuous",
        "session"=>21900,
        "prev_close"=>"13790.00000000",
        "last"=>last,
        "current_time"=>current_time,
        "bid"=>bid,
        "ask"=>ask
      }
    end
    let(:bid) { "13720.00000000" }
    let(:ask) { "13760.00000000" }
    let(:last) { "13740.00000000" }
    let(:current_time) { "2020-03-05T23:07:18.998591Z" }
    let(:currency_name) { 'btcaud' }
    let(:status) { true }
    let(:adapted_params) do
      {
        bid: bid,
        ask: ask,
        last: last,
        name: currency_name,
        current_time: current_time
      }
    end

    before do
      allow(CoinJar::Exchange).to receive(:get_currency).with(name: currency_name).
        and_return(faraday_response)
      allow(Currencies::Adapters::CoinJar).to receive(:adapt).with(payload, currency_name).
        and_return(adapted_params)
    end

    it 'creates currency' do
      expect { described_class.import_currency(currency_name) }.to change { Currency.count }.
        from(0).to(1)
    end
  end
end
