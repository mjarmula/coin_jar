require 'rails_helper'

describe Currencies::Adapters::CoinJar do
  let(:payload) do
    {
      "volume_24h"=>"46.50400000",
      "volume"=>"16.82600000",
      "transition_time"=>"2020-03-05T23:50:00Z",
      "status"=>"continuous",
      "session"=>21900,
      "prev_close"=>"13790.00000000",
      "last"=>last,
      "current_time"=>current_time,
      "bid"=>bid,
      "ask"=>ask
    }
  end

  let(:bid) { "13720.00000000" }
  let(:ask) { "13760.00000000" }
  let(:last) { "13740.00000000" }
  let(:current_time) { "2020-03-05T23:07:18.998591Z" }
  let(:currency_name) { 'btcaud' }

  let(:adapted_params) do
    {
      bid: bid,
      ask: ask,
      last: last,
      name: currency_name,
      current_time: current_time
    }
  end

  describe '.adapt' do
    it 'adapts coin jar payload to model' do
      expect(described_class.adapt(payload, currency_name)).to eq(adapted_params)
    end
  end
end
