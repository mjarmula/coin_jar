require 'rails_helper'

describe Currencies::Import do
  describe '.call' do
    let(:importer) { Currencies::Importers::CoinJar }
    let(:bid) { "13720.00000000" }
    let(:ask) { "13760.00000000" }
    let(:last) { "13740.00000000" }
    let(:current_time) { "2020-03-05T23:07:18.998591Z" }
    let(:currency_name) { 'btcaud' }

    before do
      Currency::DEFINED_NAMES = [currency_name]
    end

    context 'when there are no currencies' do
      it 'imports new currency from importer' do
        expect(importer).to receive(:import_currency).with(currency_name)

        described_class.call
      end
    end

    context 'when there is a currency created before current time' do
      before do
        create(
          :currency,
          current_time: Time.zone.now - 5.days,
          ask: ask,
          bid: bid,
          name: currency_name,
          last: last
        )
      end

      it 'creates new currency' do
        expect(importer).to receive(:import_currency).with(currency_name)

        described_class.call
      end
    end

    context 'when there is a currency created after current time' do
      before do
        create(
          :currency,
          current_time: Time.zone.now + 5.days,
          ask: ask,
          bid: bid,
          name: currency_name,
          last: last
        )
      end

      it 'uses already existing currency' do
        expect(importer).not_to receive(:import_currency)

        described_class.call
      end
    end
  end
end
