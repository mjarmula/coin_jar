require 'rails_helper'

describe CoinJar::Http::Exchanges::Get do
  include_context 'coin jar get request'

  let (:expected_response) do
    {
      "volume_24h"=>"46.50400000",
      "volume"=>"16.82600000",
      "transition_time"=>"2020-03-05T23:50:00Z",
      "status"=>"continuous", "session"=>21900,
      "prev_close"=>"13790.00000000", "last"=>"13740.00000000",
      "current_time"=>"2020-03-05T23:07:18.998591Z",
      "bid"=>"13720.00000000",
      "ask"=>"13760.00000000"
    }.to_json
  end

  it 'performes http request' do
    stub_request(:get, expected_url).to_return(status: success_status, body: stubbed_response)

    expect(described_class.call(name).body).to eq(expected_response)
  end
end
