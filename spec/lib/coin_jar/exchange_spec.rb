require 'rails_helper'

describe CoinJar::Exchange do
  describe '.get_currency' do
    let(:name) { 'btcaud' }

    it 'performs http request to pull the latest exchange' do
      expect(CoinJar::Http::Exchanges::Get).to receive(:call).with(name)

      described_class.get_currency(name: name)
    end
  end
end
