require 'rails_helper'

describe Currency, type: :model do
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:ask) }
  it { is_expected.to validate_presence_of(:bid) }
  it { is_expected.to validate_presence_of(:last) }
  it { is_expected.to validate_presence_of(:current_time) }
end
