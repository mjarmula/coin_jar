module Api
  module V1
    class CurrenciesController < ApplicationController
      def create
        if imported_currencies.success?
          render json: imported_currencies.currencies,
                 each_serializer: CurrencyCollectionSerializer,
                 status: :created
        else
          render json: { error: I18n.t('currency.import.error') },
                 status: :unprocessable_entity
        end
      end

      def index
        render json: Currency.order(current_time: :desc),
               each_serializer: CurrencyCollectionSerializer,
               status: :ok
      end

      private

      def imported_currencies
        @imported_currencies ||= Currencies::Import.call
      end
    end
  end
end
