module Currencies
  module Importers
    class CoinJar
      attr_reader :currency_name
      private :currency_name

      def initialize(currency_name)
        @currency_name = currency_name
      end

      def self.import_currency(currency_name)
        new(currency_name).import_currency
      end

      def import_currency
        Currency.create(adapted_params)
      end

      private

      def adapted_params
        ::Currencies::Adapters::CoinJar.adapt(payload, currency_name)
      end

      def payload
        if fetched_currency.success?
          JSON.parse(fetched_currency.body)
        else
          # Notify
          {}
        end
      end

      def fetched_currency
        @fetched_currency ||= ::CoinJar::Exchange.get_currency(name: currency_name)
      end
    end
  end
end
