module Currencies
  module Adapters
    class CoinJar
      attr_reader :payload, :name
      private :payload, :name

      def initialize(payload, name)
        @payload = payload
        @name = name
      end

      def self.adapt(payload, name)
        new(payload, name).adapt
      end

      def adapt
        {
          ask: payload['ask'],
          bid: payload['bid'],
          last: payload['last'],
          name: name,
          current_time: payload['current_time']
        }
      end
    end
  end
end
