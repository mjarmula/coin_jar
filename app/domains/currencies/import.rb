module Currencies
  class Import
    attr_reader :importer, :currencies
    private :importer

    def initialize(importer = Currencies::Importers::CoinJar)
      @currencies = []
      @importer = importer
    end

    def self.call
      new.call
    end

    def call
      tap do
        import_currencies
      end
    end

    def success?
      @currencies.all?(&:persisted?)
    end

    private

    def import_currencies
      Currency::DEFINED_NAMES.each do |currency_name|
        @currencies << currency(currency_name)
      end
    end

    def currency(currency_name)
      existing_currency(currency_name) || imported_currency(currency_name)
    end

    def existing_currency(currency_name)
      Currency.find_by('currencies.current_time >= ?', Time.zone.now)
    end

    def imported_currency(currency_name)
      importer.import_currency(currency_name)
    end
  end
end
