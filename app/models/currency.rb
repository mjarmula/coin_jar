class Currency < ApplicationRecord
  DEFINED_NAMES = %w[btcaud ethaud].freeze

  validates :name, :ask, :bid, :last, :current_time, presence: true
end
