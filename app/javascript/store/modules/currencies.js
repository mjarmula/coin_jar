import Api from '../../modules/api'
const api = new Api()

const state = {
  currencies: []
}

const mutations = {
  setCurrencies (state, list) {
    state.currencies = list
  }
}

const getters = {
  getCurrencies: state => {
    return state.currencies
  }
}

const actions = {
  async fetchCurrencies({ commit }) {
    const { data } = await api.client.get('/v1/currencies')

    commit('setCurrencies', data)
  },

  async captureCurrencies({ commit }) {
    await api.client.post('/v1/currencies')
  }
}

export default {
    state,
    mutations,
    actions,
    getters
}
