import axios from 'axios'

export const getCSRFToken = () => {
  return document.getElementsByName('csrf-token')[0]
    ? document.getElementsByName('csrf-token')[0].getAttribute('content')
    : null
}

const routes = {
  baseURL: '',
  basePath: '/api'
}

class Api {
  constructor () {
    const csrfToken = getCSRFToken()

    this.resourceURL = routes.baseURL + routes.basePath
    this.client = axios.create({
      baseURL: this.resourceURL,
      headers: {
        'X-CSRF-TOKEN': csrfToken
      }
    })
  }
}

export default Api
