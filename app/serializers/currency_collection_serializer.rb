class CurrencyCollectionSerializer < ActiveModel::Serializer
  attributes :bid, :ask, :last, :name
end
