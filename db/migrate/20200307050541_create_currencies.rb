class CreateCurrencies < ActiveRecord::Migration[5.2]
  def change
    create_table :currencies do |t|
      t.string :name, null: false
      t.decimal :ask, precision: 16, scale: 8, null: false
      t.decimal :bid, precision: 16, scale: 8, null: false
      t.decimal :last, precision: 16, scale: 8, null: false
      t.datetime :current_time, null: false

      t.timestamps
      t.index :current_time
    end
  end
end
