# Coin Jar - Programming challenge
Write a simple web app in Ruby or Elixir showing price history of BTC and ETH on
CoinJar Exchange. The application should:
● Capture the prices when a "Capture" button is triggered. This should save
prices of 2 currencies into the database. (Tip: you want to capture the freshest
prices as possible here)
● View a list of currencies and the latest prices.
● Click a link and see the history of captured prices of a currency in descending
order (by time).
● Capture the price of last, bid and ask.
## Getting Started
### Prerequisites
ruby >= 2.6.2
Postgres >= 11.5

### Prepare
```
bundle exec rails db:create
bundle exec rails db:migrate
bundle install
yarn install
```
### Run
```
bundle exec rails s
```

### How to test
```
rspec
```
