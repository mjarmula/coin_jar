module CoinJar
  module Exchange
    module_function

    def get_currency(name:)
      CoinJar::Http::Exchanges::Get.call(name)
    end
  end
end
