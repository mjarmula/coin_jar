module CoinJar
  module Http
    module Exchanges
      class Get
        attr_reader :name
        private :name

        def initialize(name)
          @name = name.upcase
        end

        def self.call(name)
          new(name).call
        end

        def call
          response
        end

        private

        def response
          connection.get do |req|
            req.url URI.join(base_url, path)
          end
        end

        def connection
          Faraday.new do |conn|
            conn.adapter Faraday.default_adapter
          end
        end

        def path
          "/products/#{name}/ticker"
        end

        def base_url
          'https://data.exchange.coinjar.com'
        end
      end
    end
  end
end
